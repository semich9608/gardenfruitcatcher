<?php

namespace App\Classes;

use App\Classes\Fruits\Apple;
use App\Classes\Fruits\Pear;
use App\Classes\Trees\AppleTree;
use App\Classes\Trees\PearTree;

class Garden
{
    private const COUNT_APPLE_TREES = 10;

    private const COUNT_PEAR_TREES = 15;

    private array $appleTrees;

    private array $pearTrees;

    private array $apples;

    private array $pears;

    private int $applesWeight = 0;

    private int $pearsWeight = 0;

    public function __construct()
    {
        $this->addTrees();
        $this->setApples();
        $this->setPears();
    }

    public function addTrees()
    {
        //Отсчет итераций начинается с 1, а не с 0 (как принято) для того, чтобы дать регистрационные номера деревьям
        //наичная с 1, а не с 0.
        for ($i = 1; $i <= self::COUNT_APPLE_TREES; $i++) {
            $this->appleTrees[] = new AppleTree($i);
        }

        for ($i = 1; $i <= self::COUNT_PEAR_TREES; $i++) {
            $this->pearTrees[] = new PearTree($i);
        }
    }

    public function setApples(): void
    {
        foreach ($this->getAppleTrees() as $appleTree) {
            foreach ($appleTree->getApples() as $apple) {
                $this->apples[] = $apple;
            }
        }
    }

    public function setPears(): void
    {
        foreach ($this->getPearTrees() as $pearTree) {
            $pears = $pearTree->getPears();
            if ($pears === null)
                continue;

            foreach ($pearTree->getPears() as $pear) {
                $this->pears[] = $pear;
            }
        }
    }

    public function getApples(): array
    {
        return $this->apples;
    }

    public function getPears(): array
    {
        return $this->pears;
    }

    public function getTotalNumberOfApples(): int
    {
        return count($this->getApples());
    }

    public function getTotalNumberOfPears(): int
    {
        return count($this->getPears());
    }

    public function getTotalApplesWeight(): int
    {
        foreach ($this->getApples() as $apple) {
            if ($apple instanceof Apple)
                $this->applesWeight += $apple->getWeight();
        }
        return $this->applesWeight;
    }

    public function getTotalPearsWeight(): int
    {
        foreach ($this->getPears() as $pear) {
            if ($pear instanceof Pear)
                $this->pearsWeight += $pear->getWeight();
        }
        return $this->pearsWeight;
    }

    public function getAppleTrees(): array
    {
        return $this->appleTrees;
    }

    private function getPearTrees(): array
    {
        return $this->pearTrees;
    }
}