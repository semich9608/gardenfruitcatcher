<?php

namespace App\Classes\Trees;

abstract class Tree
{
    protected int $regNumber;

    public function __construct(int $regNumber)
    {
        $this->setRegNumber($regNumber);
    }

    /**
     * @param int $regNumber
     */
    protected function setRegNumber(int $regNumber): void
    {
        $this->regNumber = $regNumber;
    }

    /**
     * @return int
     */
    public function getRegNumber(): int
    {
        return $this->regNumber;
    }

}