<?php

namespace App\Classes\Trees;

use App\Classes\Fruits\Pear;

class PearTree extends Tree
{
    private ?array $pears = null;

    public function __construct(int $regNumber)
    {
        parent::__construct($regNumber);
        $this->initializePearTree();
    }

    private function initializePearTree()
    {
        $counter = rand(0, 20);
        for ($i = 0; $i !== $counter; $i++) {
            $this->pears[] = new Pear();
        }
    }

    public function getPears(): ?array
    {
        return $this->pears;
    }
}