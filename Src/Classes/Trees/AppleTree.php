<?php

namespace App\Classes\Trees;

use App\Classes\Fruits\Apple;

class AppleTree extends Tree
{
    private array $apples;

    public function __construct(int $regNumber)
    {
        parent::__construct($regNumber);
        $this->initializeAppleTree();
    }

    private function initializeAppleTree()
    {
        $counter = rand(40, 50);
        for ($i = 0; $i !== $counter; $i++) {
            $this->apples[] = new Apple();
        }
    }

    public function getApples(): array
    {
        return $this->apples;
    }
}