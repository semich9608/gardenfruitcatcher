<?php

namespace App\Classes\Fruits;

abstract class Fruit
{
    protected int $weight;

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    protected function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }
}