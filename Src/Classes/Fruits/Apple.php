<?php

namespace App\Classes\Fruits;

class Apple extends Fruit
{
    public function __construct()
    {
        $this->setWeight(rand(150, 180));
    }
}