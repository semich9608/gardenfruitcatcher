<?php

use App\Classes\Garden;

require_once __DIR__ . '/vendor/autoload.php';

const TOTAL_FRUITS = 'В саду найдено %u яблок(-а, -о) и %u груш(-а, -и).' . PHP_EOL;
const TOTAL_WEIGHT = 'Общий вес яблок составляет %u г. (~%u кг.), а груш - %u г. (~%u кг.).' . PHP_EOL;

$garden = new Garden();
echo sprintf(TOTAL_FRUITS, $garden->getTotalNumberOfApples(), $garden->getTotalNumberOfPears());
$totalApplesWeight = $garden->getTotalApplesWeight();
$totalPearsWeight = $garden->getTotalPearsWeight();
echo sprintf(TOTAL_WEIGHT,
    $totalApplesWeight,
    $totalApplesWeight / 1000,
    $totalPearsWeight,
    $totalPearsWeight / 1000);
